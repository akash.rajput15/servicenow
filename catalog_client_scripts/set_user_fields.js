function onChange(control, oldValue, newValue, isLoading) {
    if (newValue !== '') { //if newvalue is not blank exit
        return;
    } else { //if newvalue is blank, set the other fields?
        g_form.clearValue('user_name');
        g_form.clearValue('user_email');
        g_form.clearValue('user_phone');
    }
    console.log('getting references');
    var gr = g_form.getReference('user', populateUser);
}

function populateUser(user) {
    if (user.name != '') {
        g_form.setValue('user_location', user.location);
        g_form.setValue('user_email', user.email);
        g_form.setValue('user_phone', user.phone);
    }
}