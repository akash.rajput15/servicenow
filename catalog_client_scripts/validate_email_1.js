function onChange(control, oldValue, newValue, isLoading) {
    if (isLoading || newValue === '') {
        return;
    } else { //if newvalue is blank, set the other fields?
        g_form.hideFieldMsg(control, true);
        isEmailValid(control, newValue);
    }
}

function isEmailValid(control, value) {
    var problemMsg = isEmailValidWithReason(value);
    if (problemMsg !== "") {
        //jslog("isEmailValid: " + problemMsg);
        g_form.showFieldMsg(control, problemMsg, 'error', true);
        return false;
    }
    return true;
}


function isEmailValidWithReason(value) {
    var localPartChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!#$%*/?|^{}`~&'+-=_.";
    var domainChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.";
    if (value.indexOf("@") == -1)
        return "missing @ sign";
    var s = value.split("@");
    if (s.length != 2)
        return "too many at signs";
    if (!containsOnlyChars(localPartChars, s[0]))
        return "invalid character before the at sign";
    if (s[0].length < 1)
        return "at least one character must be before the at sign";
    if (s[0].substr(0, 1) == ".")
        return "period cannot be the first character";
    if (s[0].substr(s[0].length - 1, 1) == ".")
        return "period cannot be the last character before the at sign";
    if (!containsOnlyChars(domainChars, s[1]))
        return "invalid character after the at sign";
    var periodIndex = s[1].indexOf(".");
    if (periodIndex == -1)
        return "missing period after the at sign";
    if (periodIndex === 0)
        return "period cannot be the first character after the at sign";
    var periods = s[1].split(".");
    var lastPeriod = periods[periods.length - 1];
    if (lastPeriod.length < 1)
        return "must be at least 1 character after the last period";
    if (!isAlphaNum(s[1].substr(0, 1)))
        return "the first character after the at sign must be alphanumeric";
    if (!isAlphaNum(s[1].substr(s[1].length - 1, 1)))
        return "the last character must be alphanumeric";
    return "";
}


function isAlpha(thchar) {
    return (thchar >= 'a' && thchar <= 'z\uffff') || (thchar >= 'A' && thchar <= 'Z\uffff') || thchar == '_';
}

function isAlphaNum(thchar) {
    return isAlpha(thchar) || isDigit(thchar);
}

function isDigit(thchar) {
    return (thchar >= '0' && thchar <= '9');
}

function containsOnlyChars(validChars, sText) {
    if (!sText)
        return true;
    for (var i = 0; i < sText.length; i++) {
        var c = sText.charAt(i);
        if (validChars.indexOf(c) == -1)
            return false;
    }
    return true;
}