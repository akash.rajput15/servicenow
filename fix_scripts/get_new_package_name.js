var sysattachment = "new Packages.com.glide.ui.SysAttachment();";
var zip = "new Packages.java.util.zip.ZipOutputStream();";

function getNewMethod(script) {
    var gcsf = GlideCustomerScriptFixer(script);
    gcsf.processScript();
    gcsf.convertedScript.toString();
    var returnObj = {
        script: gcsf.convertedScript,
        message: gcsf.getMessages()
    };
    return returnObj;
}

gs.print('Attachment(script): ' + getNewMethod(sysattachment).script);
gs.print('Attachment(message): ' + getNewMethod(sysattachment).message);
gs.print('zip (script): ' + getNewMethod(zip).script);
gs.print('zip (message): ' + getNewMethod(zip).message);
/*
 *** Script: Attachment(script): new GlideSysAttachment();
 *** Script: Attachment(message): []
 *** Script: zip (script): new Packages.java.util.zip.ZipOutputStream();
 *** Script: zip (message): []
 */