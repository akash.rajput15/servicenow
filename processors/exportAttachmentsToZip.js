(function process(g_request, g_response, g_processor) {
	var sysid = g_request.getParameter('sysparm_sys_id');
	var table = g_request.getParameter('sysparm_table');
	var theRecord = new GlideRecord(table);
	theRecord.get(sysid);
	var zipName = theRecord.getDisplayValue() + '.zip';
	var gr = new GlideRecord('sys_attachment');
	gr.addQuery('table_sys_id', theRecord.sys_id);
	gr.addQuery('table_name', theRecord.getTableName());
	gr.query();
	if (gr.hasNext()){
		g_response.setHeader('Pragma', 'public');
		g_response.addHeader('Cache-Control', 'max-age=0');
		g_response.setContentType('application/octet-stream');
		g_response.addHeader('Content-Disposition', 'attachment;filename=' + zipName);
		var out = new Packages.java.util.zip.ZipOutputStream(g_response.getOutputStream());
		//var out = GlideZipOutputStream(g_response.getOutputStream())
		while (gr.next()){
			var sa = new GlideSysAttachment();
			var binData = sa.getBytes(gr);
			//var binData = sa.getContent(gr); /* getContent is limited to 5mb */
			var file = gr.file_name;
			addBytesToZip(out, file, binData);
		}
		// Complete the ZIP file
		out.close();
	}
	function addBytesToZip (out, file, stream){
		// Add ZIP entry to output stream.
		out.putNextEntry(new Packages.java.util.zip.ZipEntry(file));
		//out.putNextEntry(new GlideZipEntry(file));
		out.write(stream, 0, stream.length);
		out.closeEntry();
	}
})(g_request, g_response, g_processor);
